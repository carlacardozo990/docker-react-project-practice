import React from 'react';
import './App.css';

function HelloWorld() {
  
  function sayHello()  {
    alert('Hello World!');
  }
  
  return (
    <div>
      <h1>Hello</h1>
      <button onClick={sayHello}>Click me!</button>
    </div>
  );
}

export default HelloWorld;
